require("dotenv").config();

module.exports = (eleventyConfig) => {
  eleventyConfig.addPassthroughCopy("src/style.css");
  eleventyConfig.addPassthroughCopy("src/images/");
  console.log("env: ", process.env.BASE_URL);
  return {
    baseUrl: process.env.BASE_URL || "/bsi-site",
    dir: {
      input: "src",
      output: "public",
    },
  };
};
