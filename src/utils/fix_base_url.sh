#!/bin/sh

find public ! -name "$(printf "*\n*")" -name '*.html' |
while IFS= read -r file
do
  echo "Editing file #$file"
  sed -i '' -e 's/href=\"/href\=\"\/bsi-site/g' "$file"
  sed -i '' -e 's/src=\"/src\=\"\/bsi-site/g' "$file"
done 
echo "Played $count files"